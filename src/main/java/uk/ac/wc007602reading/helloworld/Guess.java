package uk.ac.wc007602reading.helloworld;

import java.util.Random;

/**
 * Created by Owner on 23/01/2018.
 */

public class Guess {

    private int valueToGuess;
    private Random rand = new Random();     // random generator

    /**
     * default constructor
     */
    public Guess(){
        setNewValue();
    }

    /**
     * method to set new value for valueToGuess
     */
    public void setNewValue(){
        // declare valueToGuess here
        valueToGuess = rand.nextInt(10);
        System.out.println(valueToGuess);
    }

    /**
     * this function is called in a loop until the user inputs X as the correct value
     * @param X
     * @return true if guess X == value, else return false
     */
    public boolean makeGuess(int X){
        if(X == valueToGuess){
            System.out.println("CORRECT.");
            return true;
        } else {
            System.out.println("wrong");
            return false;
        }
    }


}
