package uk.ac.wc007602reading.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // bring in the Guess class
    private Guess gs = new Guess();
    private int input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView mainTxt = (TextView)findViewById(R.id.mainTxt);
        mainTxt.setText(R.string.guess_string);

        final Button btn = (Button)findViewById(R.id.btn);
        btn.setText(R.string.guess_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText et = (EditText)findViewById(R.id.inputGuess);
                input = Integer.parseInt(et.getText().toString());

                if(gs.makeGuess(input) == true){
                    mainTxt.setText(R.string.correct_guess);
                    btn.setText(R.string.play_again);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // if clicked again, reset game
                            mainTxt.setText(R.string.guess_string);
                            gs = new Guess();
                            btn.setText(R.string.guess_button);
                        }
                    });

                } else {
                    mainTxt.setText(R.string.wrong_guess);
                }
            }
        });



    }

}
